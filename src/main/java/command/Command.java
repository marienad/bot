package command;
/**
 * Суперкласс реализующий объект Команда
 */

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.Message;
import vk.VKManager;
import vk.VKServer;

import java.sql.*;

public class Command {
    public static Connection c;
    public static Statement statement;
    public Integer id;

    /**
     * Реализует команду
     * @param message сообщение
     */
    public void exec(Message message) throws SQLException, ApiException, ClientException, InterruptedException {
    }

    /**
     * Отправляет пользователю сообщение о неверном номере задачи
     *
     * @param message сообщение
     */
    public void printErr(Message message) throws ApiException {
        new VKManager().sendMessage("Такой задачи не существует \uD83D\uDE13", message.getUserId());
    }
    /**
     * Отправляет пользователю сообщение о неверной команде
     *
     * @param message сообщение
     */
    public void printCommandErr(Message message) throws ApiException {
        new VKManager().sendMessage("Такой команды не существует \uD83D\uDE13", message.getUserId());
    }

    /**
     * Возвращает сообщение полдученное от пользователя
     *
     * @return сообщение
     */
    public String getMessage() throws ApiException, ClientException, InterruptedException {
        String s = null;
        while (s == null) {
            VKServer.getMessage();
            try {
                if (!VKServer.message.getUserId().equals(id)) {
                    throw new NullPointerException("id error");
                }
                s = VKServer.message.getBody();
            } catch (NullPointerException e) {
                Thread.sleep(200);
            }
        }
        return s;
    }

    /**
     * Подключается к базе данных
     */
    public static void connect() {
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/Data",
                            "postgres", "123");
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Opened database successfully");
        try {
            statement = c.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}
