package command.utils;
/**
 * Класс реализующий команду "Показать задачи"
 */
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.objects.messages.Message;
import command.Command;
import vk.VKManager;

import java.sql.*;

public class TaskList extends Command {
    /**
     * Реализует команду "Показать задачу\и"
     *
     * @param message сообщение
     */
    @Override
    public void exec(Message message) throws SQLException, ApiException {
        connect();
        String s = "";
        ResultSet text = statement.executeQuery("SELECT * FROM \"Tasks\" WHERE userid=" + message.getUserId());
        int i = 1;
        while (text.next()) {
            String n = text.getBoolean(text.findColumn("notif"))
                    ? " \uD83D\uDD14 " + text.getString(text.findColumn("day")) + " " + text.getString(text.findColumn("time")) : " \uD83D\uDD15 ";
            s += i + ". " + text.getString(text.findColumn("body")) + n + "\n";
            i++;
        }
        try {
            new VKManager().sendMessage(s, message.getUserId());
        } catch (ApiException e) {
            new VKManager().sendMessage("У вас пока нет задач, чтобы создать введите команду \'создать задачу\'", message.getUserId());
        }
    }
}
