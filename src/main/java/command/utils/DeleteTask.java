package command.utils;
/**
 * Класс реализующий команду "Удалить задачу"
 */

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.Message;
import command.Command;
import vk.VKManager;

import java.sql.*;

public class DeleteTask extends Command {
    /**
     * Реализует команду "Удалить задачу"
     *
     * @param message сообщение
     */
    @Override
    public void exec(Message message) throws SQLException, ApiException, ClientException, InterruptedException {
        connect();
        id = message.getUserId();
        int n = Integer.parseInt(message.getBody().split(" ")[2]);
        if (n < 0) {
            printErr(message);
        }
        ResultSet text = statement.executeQuery("SELECT * FROM \"Tasks\" WHERE userid=" + message.getUserId());
        text.absolute(n);
        if (text.getRow() == 0) {
            printErr(message);
            return;
        }
        new VKManager().sendMessage("Уверены что хотите удалить?(да/нет)", message.getUserId());
        switch (getMessage().toLowerCase()) {
            case "да":
                statement.executeUpdate("DELETE FROM \"Tasks\" WHERE id=" + text.getInt(text.findColumn("id")));
                new VKManager().sendMessage("Задача удалена ✅", message.getUserId());
                break;
            case "нет":
                new VKManager().sendMessage("ну нет так нет", message.getUserId());
                return;
            default:
                printCommandErr(message);
        }
    }
}
