package command.utils;
/**
 * Класс реализующий команду "Изменить задачу"
 */
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.Message;
import command.Command;
import vk.VKManager;

import java.sql.*;

public class ChangeTask extends Command {
    /**
     * Реализует команду "Изменить задачу"
     *
     * @param message сообщение
     */
    @Override
    public void exec(Message message) throws SQLException, ApiException {
        connect();
        id = message.getUserId();
        int n = Integer.parseInt(message.getBody().split(" ")[2]);
        if (n < 0) {
            printErr(message);
            return;
        }
        ResultSet text = statement.executeQuery("SELECT * FROM \"Tasks\" WHERE userid=" + message.getUserId());
        text.absolute(n);
        if (text.getRow() == 0) {
            printErr(message);
            return;
        }
        new VKManager().sendMessage("Что ты хочешь изменить? \n 1. Описание задачи ✍  \n 2. Настройки напоминаний \uD83D\uDD14 \n 3. Не изменять задачу", message.getUserId());
        try {
            int i = Integer.parseInt(getMessage());
            switch (i) {
                case 1:
                    new VKManager().sendMessage("Введите новое описание", message.getUserId());
                    String s = getMessage();
                    statement.executeUpdate("UPDATE \"Tasks\" SET body=\'" + s + "\' WHERE id=" + text.getInt(text.findColumn("id")));
                    break;
                case 2:
                    new VKManager().sendMessage("Что ты хочешь изменить? \n 1. Вкл/выкл напоминание \n 2. Изменить дату и время \n 3. Не изменять задачу", message.getUserId());
                    int h = Integer.parseInt(getMessage());
                    switch (h) {
                        case 1:
                            boolean b = !text.getBoolean(text.findColumn("notif"));
                            statement.executeUpdate("UPDATE \"Tasks\" SET notif=" + b + " WHERE id=" + text.getInt(text.findColumn("id")));
                            break;
                        case 2:
                            new VKManager().sendMessage("Напиши мне дату в формате дд.мм.гггг \uD83D\uDCC5", message.getUserId());
                            String date = getMessage();
                            new VKManager().sendMessage("А теперь время в формате чч:мм \uD83D\uDD5A", message.getUserId());
                            String time = getMessage();
                            try {
                                statement.executeUpdate("UPDATE \"Tasks\" SET day=\'" + date + "\', time=\'" + time + "\' WHERE id=" + text.getInt(text.findColumn("id")));
                            } catch (SQLException e) {
                                new VKManager().sendMessage("Вводите дату и время корректно, я могу и обидиться (｡•́︿•̀｡)", message.getUserId());
                                return;
                            }
                            break;
                        case 3:
                            new VKManager().sendMessage("ну нет так нет", message.getUserId());
                            return;
                        default:
                            printCommandErr(message);
                            return;
                    }
                    break;
                case 3:
                    new VKManager().sendMessage("ну нет так нет", message.getUserId());
                    return;
                default:
                    printCommandErr(message);
                    return;
            }
            new VKManager().sendMessage("Задача изменена ✅", message.getUserId());
        } catch (ApiException | ClientException | InterruptedException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            printCommandErr(message);
        }
    }
}
