package command.utils;
/**
 * Класс реализующий команду "Создать задачу"
 */
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.Message;
import command.Command;
import vk.VKManager;

import java.sql.SQLException;

public class CreateTask extends Command {
    /**
     * Реализует команду "Создать задачу"
     *
     * @param message сообщение
     */
    @Override
    public void exec(Message message) throws ApiException, ClientException, InterruptedException {
        connect();
        id = message.getUserId();
        new VKManager().sendMessage("Расскажи мне что ты хочешь сделать", message.getUserId());
        String body = getMessage();
        new VKManager().sendMessage("Тебе нужно напомнить?(да/нет)", message.getUserId());
        String not ;
        switch (getMessage().toLowerCase()) {
            case "да":
                not = "true";
                new VKManager().sendMessage("Напиши мне дату в формате дд.мм.гггг \uD83D\uDCC5", message.getUserId());
                String date = getMessage();
                new VKManager().sendMessage("А теперь время в формате чч:мм \uD83D\uDD5A", message.getUserId());
                String time = getMessage();
                try {
                    statement.executeUpdate("INSERT INTO \"Tasks\" (userid, body, day, \"time\", notif) VALUES (" + message.getUserId() + ",\'" + body + "\',\'" + date + "\',\'" + time + "\'," + not + ");");
                } catch (SQLException e) {
                    new VKManager().sendMessage("Вводите дату и время корректно, я могу и обидиться (｡•́︿•̀｡)", message.getUserId());
                    return;
                }
                new VKManager().sendMessage("Задача добавлена ✅", message.getUserId());
                break;
            case "нет":
                not = "false";
                try {
                    statement.executeUpdate("INSERT INTO \"Tasks\" (userid, body, notif) VALUES (" + message.getUserId() + ",\'" + body + "\'," + not + ");");
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                new VKManager().sendMessage("Задача добавлена ✅", message.getUserId());
                break;
            default:
                printCommandErr(message);
        }
    }
}
