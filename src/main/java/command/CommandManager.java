package command;
/**
 * Класс предназначенный для обработки команд
 */

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.Message;
import command.utils.*;
import vk.VKManager;

import java.io.IOException;
import java.sql.SQLException;

public class CommandManager {
    /**
     * Обрабатывает команду пришедшую в сообщении
     *
     * @param message сообщение
     */
    public static void exec(Message message) throws ApiException {
        String body = message.getBody().split("-?\\d")[0].trim().toLowerCase();
        switch (body) {
            case "начать":
                new VKManager().sendMessage("Добро пожаловать! Меня зовут бот Мари, если хочешь посмотреть что я умею введи команду \"помоги\"" +
                        " ༼ つ ◕_◕ ༽つ", message.getUserId());
                break;
            case "погода":
                new VKManager().sendMessage(getWeather(), message.getUserId());
                break;
            case "помоги":
                new VKManager().sendMessage("(っ˘ω˘ς) вот что я могу \n 。помоги - вывод всех доступных команд \n 。погода - вывод погоды на сегодня в пензе" +
                        "\n 。создать задачу - добавить задачу в список \n 。показать задачи - вывод всех текущих задач \n 。изменить задачу <номер> - измение задачи из списка" +
                        " \n 。удалить задачу <номер> - удаление задачи из списка \n \uD83C\uDF38Вводи команды верно и мы с тобой обязательно подружимся\uD83C\uDF38", message.getUserId());
                break;
            case "привет":
                new VKManager().sendMessage("(ﾉ◕ヮ◕)ﾉ*:･ﾟ✧ привет мой светлый друг ✧ﾟ･: *ヽ(◕ヮ◕ヽ)", message.getUserId());
                break;
            case "создать задачу":
                try {
                    new CreateTask().exec(message);
                } catch (ClientException | ApiException | InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            case "показать задачи":
                try {
                    new TaskList().exec(message);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "изменить задачу":
                try {
                    new ChangeTask().exec(message);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                break;
            case "удалить задачу":
                try {
                    new DeleteTask().exec(message);
                } catch (SQLException | InterruptedException | ClientException e) {
                    e.printStackTrace();
                }
                break;
            default:
                new VKManager().sendMessage("Введите команду корректно (」°ロ°)」", message.getUserId());
        }
    }

    /**
     * Возращает строку с погодой
     *
     * @return погода
     */
    private static String getWeather() {
        String weather;
        try {
            weather = new WeatherParser().getWeatherTodayDescription();
        } catch (IOException ex) {
            weather = "не удалось получить погоду";
        }
        return weather;
    }
}
