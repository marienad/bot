package vk; /**
 * Класс для связи приложения с пользователем
 */

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;

public class VKManager {
    public static VKCore vkCore;

    static {
        try {
            vkCore = new VKCore();
        } catch (ApiException | ClientException e) {
            e.printStackTrace();
        }
    }

    /**
     * Отправляет сообщение пользователю
     *
     * @param msg сообщение
     * @param peerId id пользователя
     */
    public void sendMessage(String msg, int peerId) throws ApiException {
        if (msg == null) {
            System.out.println("null");
            return;
        }
        try {
            vkCore.getVk().messages().send(vkCore.getActor()).peerId(peerId).message(msg).execute();
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
}
