package vk;

/**
 * Класс реализующий обьект-месенджер
 */

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.objects.messages.Message;
import command.CommandManager;

public class Messenger implements Runnable {
    private static Message message;
    private static int id;

    public Messenger(Message message) {
        this.message = message;
        this.id = message.getUserId();
    }

    public static int getId() {
        return id;
    }

    @Override
    public void run() {
        try {
            CommandManager.exec(message);
        } catch (ApiException e) {
            e.printStackTrace();
        }
        VKServer.users.remove((Integer)id);
    }
}
