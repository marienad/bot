package vk;

/**
 * Класс реализующий сервер-приложения
 */

import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.objects.messages.Message;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class VKServer {
    private static VKCore vkCore;
    private static String[] com = {"помоги", "начать", "погода", "привет", "создать задачу", "показать задачи", "изменить задачу -?\\w\\w?", "удалить задачу -?\\w\\w?"};
    public static Message message;
    public static List<Integer> users;

    static {
        try {
            vkCore = new VKCore();
            users = new ArrayList<>();
        } catch (ApiException | ClientException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws NullPointerException, ApiException, InterruptedException {
        System.out.println("Running server...");
        while (true) {
            Thread.sleep(300);
            try {
                getMessage();
                if (message != null && isCommand(message) && !users.contains(message.getUserId())) {
                    ExecutorService exec = Executors.newCachedThreadPool();
                    users.add(message.getUserId());
                    exec.execute(new Messenger(message));
                }
            } catch (NullPointerException | ClientException e) {
                System.out.println("Возникли проблемы");
                final int RECONNECT_TIME = 10000;
                System.out.println("Повторное соединение через " + RECONNECT_TIME / 1000 + " секунд");
                Thread.sleep(RECONNECT_TIME);
            }
        }
    }

    /**
     * Получение сообщения от пользователя
     */
    public static void getMessage() throws ApiException, ClientException {
        message = vkCore.getMessage();
    }

    /**
     * Возвращает true , если сообщение команда
     * false, если нет
     *
     * @param m
     * @return
     */
    public static boolean isCommand(Message m) {
        for (int i = 0; i < com.length; i++) {
            if (m.getBody().toLowerCase().matches(com[i])) {
                return true;
            }
        }
        return false;
    }
}